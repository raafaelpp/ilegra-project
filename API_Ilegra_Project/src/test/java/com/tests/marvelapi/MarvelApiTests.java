package com.tests.marvelapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.comum.marvelapi.RestUtils;
import com.constantes.marvelapi.Auth;
import com.constantes.marvelapi.EndPoints;
import com.constantes.marvelapi.Path;
import com.dataproviders.marvelapi.MarvelApiTestData;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class MarvelApiTests {

	RequestSpecification reqSpec;
	ResponseSpecification resSpec;

	@BeforeClass
	public void setup() {
		reqSpec = RestUtils.getRequestSpecification();
		reqSpec.basePath(Path.BASE_PATH);
		reqSpec.queryParam("ts", Auth.TIME_STAMP);
		reqSpec.queryParam("apikey", Auth.PUBLIC_KEY);
		reqSpec.queryParam("hash", Auth.HASH);

		resSpec = RestUtils.getResponseSpecification();

	}

	@Test(dataProvider = "heroNames", dataProviderClass = MarvelApiTestData.class, enabled = true)
	public void validateCharacterName(String character) {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", character).log().all()
				.when().get(EndPoints.CHARACTERS);

		String characterName = resp.path("data.results.name[0]", character);
		Assert.assertEquals(characterName, character);

		resp.then().log().ifValidationFails().spec(resSpec);
	}

	@Test(dataProvider = "copyright", dataProviderClass = MarvelApiTestData.class, enabled = true)
	public void validateCopyright(String character, String copyright) {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", character).log().all()
				.when().get(EndPoints.CHARACTERS);
					resp.then().log().ifValidationFails().spec(resSpec)
						.and().log().ifValidationFails()
							.body("copyright", equalTo(copyright));

	}
	
	@Test(dataProvider = "heroId", dataProviderClass = MarvelApiTestData.class, enabled = true)
	public void validateHeroId(String character, int heroId) {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", character).log().all()
				.when().get(EndPoints.CHARACTERS);
					resp.then().log().ifValidationFails().spec(resSpec)
						.and().log().ifValidationFails()
							.body("data.results[0].id", equalTo(heroId));
	}
	
	@Test(dataProvider = "comicsAvailable", dataProviderClass = MarvelApiTestData.class, enabled = true)
	public void validateComicsAvailable(String character, int comicsAvailable) {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", character).log().all()
				.when().get(EndPoints.CHARACTERS);
					resp.then().log().ifValidationFails().spec(resSpec)
						.and().log().ifValidationFails()
							.body("data.results[0].comics.available", equalTo(comicsAvailable));
	}
	
	//Testes para serem falhados
	@Test(dataProvider = "seriesAvailable", dataProviderClass = MarvelApiTestData.class, enabled = true)
	public void validateSeriesAvailable(String character, int comicsAvailable) {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", character).log().all()
				.when().get(EndPoints.CHARACTERS);
					resp.then().log().ifValidationFails().spec(resSpec)
						.and().log().ifValidationFails()
							.body("data.results[0].series.available", equalTo(comicsAvailable));
	}
	
	@Test(enabled = true)
	public void statusCode() {
		Response resp = null;

		resp = given().spec(reqSpec).queryParam("name", "Wolverine")
				.when().get(EndPoints.CHARACTERS);
					resp.then().log().ifValidationFails().statusCode(401);
	}


}