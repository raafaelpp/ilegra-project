package com.testng.listeners.marvelapi;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

public class CustomListeners implements IInvokedMethodListener, ITestListener, ISuiteListener {

	@Override
	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		// Antes de todo m�todo na nossa classe de testes...
		System.out.println("Antes de iniciar o teste: " + testResult.getTestClass().getName() + " - "
				+ method.getTestMethod().getMethodName());
	}

	@Override
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		// Ap�s todo m�todo na nossa classe de testes...
		System.out.println("Ap�s o teste: " + testResult.getTestClass().getName() + " - "
				+ method.getTestMethod().getMethodName());
	}

	@Override
	public void onTestStart(ITestResult result) {
		// Quando o nosso m�todo/teste inicia...
		System.out.println("Iniciando - Nome do Teste: " + result.getName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// Executado quando nosso teste � executado com sucesso...
		System.out.println("Teste executado com sucesso! - Nome do teste: " + result.getName());
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// Executado quando o teste falha...
		System.out.println("Teste falhado - Nome do teste: " + result.getName());
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// Executado quando o teste por algum motivo � pulado...
		System.out.println("Teste pulado - Nome do teste: " + result.getName());
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// Executado quando um teste com varias valida��es � executado e apenas um percentual dessas valida��es falharam...
		System.out.println("Teste falhado - Nome do teste: " + result.getName());
	}

	@Override
	public void onStart(ITestContext context) {
		// Antes do teste ser iniciado, pega o nome do teste/suite sobre a tag <test>...
		System.out.println("Iniciando os testes - Tag Name do Teste: " + context.getName());
		ITestNGMethod methods[] = context.getAllTestMethods();
		
		System.out.println("Os seguintes m�todos/testes ser�o executados:");
		
		for (ITestNGMethod method : methods) {
			System.out.println(method.getMethodName());
		}
	}

	@Override
	public void onFinish(ITestContext context) {
		// Ap�s o teste ser finalizado, pega o nome do teste/suite sobre a tag <test>...
		System.out.println("Finalizando os testes - Tag Name do Teste: " + context.getName());
	}

	@Override
	public void onStart(ISuite suite) {
		// Antes da tag <suite> ser iniciada
		System.out.println("Iniciando: Executado antes do inicio da suite");
	}

	@Override
	public void onFinish(ISuite suite) {
		// Antes da tag <suite> � completada...
		System.out.println("Finalizando: Suite executada!");
	}

}
