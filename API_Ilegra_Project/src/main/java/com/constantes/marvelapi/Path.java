package com.constantes.marvelapi;

public class Path {
	
	public static final String BASE_URI = "https://gateway.marvel.com/";
	public static final String BASE_PATH = "/v1/public";

}
