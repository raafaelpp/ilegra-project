package com.constantes.marvelapi;

import org.apache.commons.codec.digest.DigestUtils;

public class Auth {
	
	public static final String PUBLIC_KEY = "94878d6f50291408451caeacc33b3b99";
	public static final String PRIVATE_KEY = "517c422b1123f00b06b54ca360df64a9c72995a7";
	public static final long TIME_STAMP = System.currentTimeMillis();
	
	static String toHash = TIME_STAMP + PRIVATE_KEY + PUBLIC_KEY;
	
	public static final String HASH = DigestUtils.md5Hex(toHash);
}
