package com.dataproviders.marvelapi;

import org.testng.annotations.DataProvider;

public class MarvelApiTestData {

	@DataProvider(name="heroNames")
	public Object[][] charactersNameValidation() {
		return new Object[][] {
			{"Captain America"},
			{"Iron Man"},
			{"Black Widow"},
			{"Wolverine"}
		};
	}
	
	@DataProvider(name="copyright")
	public Object[][] copyrightValidation() {
		return new Object[][] {
			{"Captain America", "� 2018 MARVEL"},
			{"Iron Man", "� 2018 MARVEL"},
			{"Black Widow", "� 2018 MARVEL"},
			{"Wolverine", "� 2018 MARVEL"}
		};
	}
	
	@DataProvider(name="heroId")
	public Object[][] heroId() {
		return new Object[][] {
			{"Captain America", 1009220},
			{"Iron Man", 1009368},
			{"Black Widow", 1009189},
			{"Wolverine", 1009718}
		};
	}
	
	@DataProvider(name="comicsAvailable")
	public Object[][] comicsAvailable() {
		return new Object[][] {
			{"Captain America", 2101},
			{"Iron Man", 2464},
			{"Black Widow", 464},
			{"Wolverine", 2223}
		};
	}
	
	@DataProvider(name="seriesAvailable")
	public Object[][] seriesAvailable() {
		return new Object[][] {
			{"Captain America", 10},
			{"Iron Man", 20},
			{"Black Widow", 30},
			{"Wolverine", 40}
		};
	}

	
}
