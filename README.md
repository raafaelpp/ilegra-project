# marvel-api-project

Documentação para o teste:

# Sobre > src/java/main, temos o seguinte:

# Pacote:
com.comum.marvelapi
# Classes:

RestUtils.Class:
Nessa classe eu desenvolvi o básico referente à especificação do nosso request e o básico referente à algumas validações no response do mesmo.

# Pacote:
com.constantes.marvelapi
# Classes:

Auth.Class:
Nessa classe, é onde estão guardadas as informações de autorização para realizar os requests na API da marvel, como public key, private key e a lógica para que o hash seja criado.

EndPoints.Class:
Nessa classe, é onde poderiamos cadastrar todos os endpoins necessários dos nossos requests, eu criei apenas um endpoint, pois só utilizei um em todos os testes.

Path.Class:
Nessa classe estão guardadas as informações de URI base e path base.

# Pacote:
com.dataproviders.marvelapi
Classes:

MarvelApiTestData.Class:
Nessa classe estão guardados todos os data providers utilizados nos testes.

# Pacote:
com.testng.listeners.marvelapi
# Classes:

CustomListeners.Class:
Nessa classe é onde todos os listeners estão criados.

# Sobre src/test/main, temos o seguinte: 
# Pacote:
com.tests.marvelapi
# Classes:

MarvelApiTests.Class
Nessa classe é onde estão todos os testes criados.

# Sobre "TestNG XML Files":
É onde temos o nosso XML que irá ser executado para rodarmos os testes, onde no mesmo temos a inicialização dos listeners para os testes, e também temos a execução paralela implementada.